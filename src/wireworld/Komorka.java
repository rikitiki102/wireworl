package wireworld;

public class Komorka {

	Stan stanKomorki;

	public Komorka(Stan stan) {
		stanKomorki = stan;
	}

	public void ustawiajStan(Stan stan) {
		stanKomorki = stan;
	}

	public String zwracajStan() {
		return stanKomorki.getClass().getSimpleName();
	}

	public String toString() {
		return this.zwracajStan();
	}

}
