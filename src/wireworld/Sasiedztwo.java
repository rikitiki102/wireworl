package wireworld;

public class Sasiedztwo {

	public int zwracajLiczbeGlow(int x, int y, Plansza plansza) {
		int glowy = 0;
		int i, j = 0;
		for (i = x - 1; i <= x + 1; i++) {
			if (i < 0 || i >= plansza.zwracajWiersze()) {
				continue;
			}
			for (j = y - 1; j <= y + 1; j++) {
				if ((i == x && j == y) || j < 0
						|| j >= plansza.zwracajKolumny()) {
					continue;
				}

				if (plansza.siatka[i][j].zwracajStan().equals("Glowa")) {
					glowy++;
				}
			}
		}
		return glowy;
	}

}
