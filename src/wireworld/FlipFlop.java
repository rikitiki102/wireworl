package wireworld;

public class FlipFlop implements Elementy {

	@Override
	public void narysujElement(int x, int y, Plansza plansza) {
		plansza.siatka[x][y].ustawiajStan(new Przewodnik());
		if (!(x < 0 || y - 1 < 0 || x > plansza.zwracajWiersze() - 1 || y - 1 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x][y - 1].ustawiajStan(new Przewodnik());
		}
		if (!(x < 0 || y - 2 < 0 || x > plansza.zwracajWiersze() - 1 || y - 2 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x][y - 2].ustawiajStan(new Przewodnik());
		}
		if (!(x - 1 < 0 || y - 1 < 0 || x - 1 > plansza.zwracajWiersze() - 1 || y - 1 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x - 1][y - 1].ustawiajStan(new Przewodnik());
		}
		if (!(x + 1 < 0 || y - 1 < 0 || x + 1 > plansza.zwracajWiersze() - 1 || y - 1 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x + 1][y - 1].ustawiajStan(new Przewodnik());
		}
		if (!(x - 1 < 0 || y - 3 < 0 || x - 1 > plansza.zwracajWiersze() - 1 || y - 3 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x - 1][y - 3].ustawiajStan(new Przewodnik());
		}
		if (!(x - 1 < 0 || y + 1 < 0 || x - 1 > plansza.zwracajWiersze() - 1 || y + 1 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x - 1][y + 1].ustawiajStan(new Przewodnik());
		}
		if (!(x + 1 < 0 || y + 1 < 0 || x + 1 > plansza.zwracajWiersze() - 1 || y + 1 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x + 1][y + 1].ustawiajStan(new Przewodnik());
		}
		if (!(x + 2 < 0 || y + 1 < 0 || x + 2 > plansza.zwracajWiersze() - 1 || y + 1 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x + 2][y + 1].ustawiajStan(new Przewodnik());
		}
		if (!(x - 1 < 0 || y + 2 < 0 || x - 1 > plansza.zwracajWiersze() - 1 || y + 2 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x - 1][y + 2].ustawiajStan(new Przewodnik());
		}
		if (!(x + 1 < 0 || y + 2 < 0 || x + 1 > plansza.zwracajWiersze() - 1 || y + 2 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x + 1][y + 2].ustawiajStan(new Przewodnik());
		}
		if (!(x < 0 || y + 3 < 0 || x > plansza.zwracajWiersze() - 1 || y + 3 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x][y + 3].ustawiajStan(new Przewodnik());
		}
		if (!(x - 1 < 0 || y + 3 < 0 || x - 1 > plansza.zwracajWiersze() - 1 || y + 3 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x - 1][y + 3].ustawiajStan(new Przewodnik());
		}
		if (!(x - 2 < 0 || y + 3 < 0 || x - 2 > plansza.zwracajWiersze() - 1 || y + 3 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x - 2][y + 3].ustawiajStan(new Przewodnik());
		}
		if (!(x - 1 < 0 || y + 4 < 0 || x - 1 > plansza.zwracajWiersze() - 1 || y + 4 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x - 1][y + 4].ustawiajStan(new Przewodnik());
		}
		if (!(x - 2 < 0 || y + 5 < 0 || x - 2 > plansza.zwracajWiersze() - 1 || y + 5 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x - 2][y + 5].ustawiajStan(new Przewodnik());
		}

	}

}
