package wireworld;

import java.awt.Color;

public class Pusta implements Stan {

	@Override
	public Stan nowyStan(int t) {

		return new Pusta();
	}

	@Override
	public Color zwracajKolor() {
		return Color.BLACK;
	}

}
