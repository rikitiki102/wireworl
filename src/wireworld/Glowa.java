package wireworld;

import java.awt.Color;

public class Glowa implements Stan {

	@Override
	public Stan nowyStan(int i) {
		return new Ogon();
	}

	@Override
	public Color zwracajKolor() {
		return Color.RED;
	}

}
