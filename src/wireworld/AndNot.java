package wireworld;

public class AndNot implements Elementy {

	@Override
	public void narysujElement(int x, int y, Plansza plansza) {
		plansza.siatka[x][y].ustawiajStan(new Przewodnik());
		if (!(x + 1 < 0 || y < 0 || x + 1 > plansza.zwracajWiersze() - 1 || y > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x + 1][y].ustawiajStan(new Przewodnik());
		}
		if (!(x - 1 < 0 || y < 0 || x - 1 > plansza.zwracajWiersze() - 1 || y > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x - 1][y].ustawiajStan(new Przewodnik());
		}
		if (!(x - 2 < 0 || y < 0 || x - 2 > plansza.zwracajWiersze() - 1 || y > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x - 2][y].ustawiajStan(new Przewodnik());
		}
		if (!(x - 1 < 0 || y + 1 < 0 || x - 1 > plansza.zwracajWiersze() - 1 || y + 1 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x - 1][y + 1].ustawiajStan(new Przewodnik());
		}
		if (!(x + 1 < 0 || y + 1 < 0 || x + 1 > plansza.zwracajWiersze() - 1 || y + 1 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x + 1][y + 1].ustawiajStan(new Przewodnik());
		}
		if (!(x + 1 < 0 || y - 1 < 0 || x + 1 > plansza.zwracajWiersze() - 1 || y - 1 > plansza
				.zwracajKolumny())) {
			plansza.siatka[x + 1][y - 1].ustawiajStan(new Przewodnik());
		}
		if (!(x + 1 < 0 || y - 2 < 0 || x + 1 > plansza.zwracajWiersze() - 1 || y - 2 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x + 1][y - 2].ustawiajStan(new Przewodnik());
		}
		if (!(x + 2 < 0 || y - 2 < 0 || x + 2 > plansza.zwracajWiersze() - 1 || y - 2 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x + 2][y - 2].ustawiajStan(new Przewodnik());
		}
		if (!(x + 1 < 0 || y - 3 < 0 || x + 1 > plansza.zwracajWiersze() - 1 || y - 3 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x + 1][y - 3].ustawiajStan(new Przewodnik());
		}
		if (!(x < 0 || y - 3 < 0 || x > plansza.zwracajWiersze() - 1 || y - 3 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x][y - 3].ustawiajStan(new Przewodnik());
		}
		if (!(x - 1 < 0 || y - 3 < 0 || x - 1 > plansza.zwracajWiersze() - 1 || y - 3 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x - 1][y - 3].ustawiajStan(new Przewodnik());
		}
		if (!(x - 1 < 0 || y - 2 < 0 || x - 1 > plansza.zwracajWiersze() - 1 || y - 2 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x - 1][y - 2].ustawiajStan(new Przewodnik());
		}
		if (!(x - 2 < 0 || y - 2 < 0 || x - 2 > plansza.zwracajWiersze() - 1 || y - 2 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x - 2][y - 2].ustawiajStan(new Przewodnik());
		}
		if (!(x - 1 < 0 || y - 1 < 0 || x - 1 > plansza.zwracajWiersze() - 1 || y - 1 > plansza
				.zwracajKolumny() - 1)) {
			plansza.siatka[x - 1][y - 1].ustawiajStan(new Przewodnik());
		}

	}

}
