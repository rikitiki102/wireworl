package wireworld;

public class Wireworld {

	public Plansza plansza;
	public Plansza plansza2;

	public Wireworld(Plansza plansza) {
		this.plansza = plansza;
		plansza2 = new Plansza(60, 60);
	}

	public Plansza nastepnaGeneracja(Plansza plansza) {
		Sasiedztwo otoczenie = new Sasiedztwo();
		Stan stan = null;
		Stan stan2 = null;
		int liczbaGlow = 0;
		for (int i = 0; i < plansza.zwracajWiersze(); i++) {
			for (int j = 0; j < plansza.zwracajKolumny(); j++) {
				liczbaGlow = otoczenie.zwracajLiczbeGlow(i, j, plansza);
				stan = plansza.pobierajStan(i, j);
				stan2 = stan.nowyStan(liczbaGlow);
				plansza2.siatka[i][j].ustawiajStan(stan2);
			}
		}
		return plansza2;
	}

}