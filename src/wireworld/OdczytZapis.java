package wireworld;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintStream;

public class OdczytZapis {

	public Plansza czytajPlik(File nazwaPliku) throws IOException {
		Plansza plansza = new Plansza(60, 60);
		BufferedReader odczyt = null;
		int i = 0;
		int j = 0;
		try {
			odczyt = new BufferedReader(new FileReader(nazwaPliku));
			String bufor;
			Fabryka fabrykaStanow = new FabrykaStanow();
			while ((bufor = odczyt.readLine()) != null) {
				String[] p = bufor.split("\\s+");
				try {
					i = Integer.parseInt(p[1]);
					j = Integer.parseInt(p[2]);
					String nazwaElementu = p[0];
					Stan stan = fabrykaStanow.buduj(nazwaElementu);
					if (stan != null) {
						plansza.siatka[i][j].ustawiajStan(stan);
					} else {
						continue;
					}
				} catch (ArrayIndexOutOfBoundsException e) {
					System.err.println("Pominieto linie \"" + bufor
							+ "\" zbyt malo p�l do odczytu");
				} catch (NumberFormatException e) {
					System.err.println("Pominieto linie \"" + bufor + "\" - \""
							+ p[2] + "\" nie jest liczba calkowita");
				}
			}
		} catch (IOException e) {
			System.err.println(e.getMessage());
		} finally {
			if (odczyt != null)
				odczyt.close();
		}
		return plansza;
	}

	public void zapisujPlik(Plansza plansza, File nazwaPliku)
			throws IOException {
		try (PrintStream wyjscie = new PrintStream(nazwaPliku)) {
			for (int i = 0; i < plansza.zwracajWiersze(); i++) {
				for (int j = 0; j < plansza.zwracajKolumny(); j++) {
					if (plansza.siatka[i][j].zwracajStan().equals("Pusta")) {
						continue;
					} else {
						wyjscie.print(plansza.siatka[j][i]);
						wyjscie.println(" " + i + " " + j);
					}
				}
			}
		}
	}
}
