package wireworld;

import java.awt.Color;

public class Ogon implements Stan {

	@Override
	public Stan nowyStan(int i) {

		return new Przewodnik();
	}

	@Override
	public Color zwracajKolor() {
		return Color.BLUE;
	}

}
