package wireworld;

import java.awt.Color;

public class Przewodnik implements Stan {

	@Override
	public Color zwracajKolor() {
		return Color.YELLOW;
	}

	@Override
	public Stan nowyStan(int i) {
		if (i == 2 || i == 1)
			return new Glowa();
		else
			return new Przewodnik();

	}

}
