package wireworld;

public class FabrykaStanow implements Fabryka {

	@Override
	public Stan buduj(String nazwaElementu) {

		switch (nazwaElementu) {
		case "Glowa":
			return new Glowa();
		case "Przewodnik":
			return new Przewodnik();
		case "Ogon":
			return new Ogon();
		case "Pusta":
			return new Pusta();
		default:
			System.err.println("Pominieto linie: \"" + nazwaElementu
					+ "\" nie istnieje taki element");
			return null;
		}

	}

}
