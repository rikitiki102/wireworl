package wireworld;

public class Plansza {

	public Komorka siatka[][];
	private int wiersze;
	private int kolumny;

	public Plansza(int w, int c) {
		wiersze = w;
		kolumny = c;
		siatka = new Komorka[w][c];
		for (int i = 0; i < w; i++)
			for (int j = 0; j < c; j++) {
				siatka[i][j] = new Komorka(new Pusta());
			}
	}

	public int zwracajWiersze() {
		return wiersze;
	}

	public int zwracajKolumny() {
		return kolumny;
	}

	public void wyczyscPlansze() {
		for (int i = 0; i < this.wiersze; i++)
			for (int j = 0; j < this.kolumny; j++) {
				this.siatka[i][j] = new Komorka(new Pusta());
			}
	}

	public Stan pobierajStan(int i, int j) {
		return this.siatka[i][j].stanKomorki;
	}

	public Plansza kopjuj(Plansza plansza) {
		Plansza plansza2 = new Plansza(60, 60);
		for (int i = 0; i < plansza.zwracajWiersze(); i++) {
			for (int j = 0; j < plansza.zwracajKolumny(); j++) {
				plansza2.siatka[i][j].stanKomorki = plansza.pobierajStan(i, j);
			}
		}
		return plansza2;
	}

}
