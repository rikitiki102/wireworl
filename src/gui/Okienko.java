package gui;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import java.awt.Color;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JRadioButton;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import wireworld.*;

public class Okienko extends JFrame implements ActionListener {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField textField;
	private JRadioButton rdbtnNewRadioButton_4, rdbtnNewRadioButton_5,
			rdbtnNewRadioButton_6, rdbtnNewRadioButton_7, rdbtnNewRadioButton,
			rdbtnNewRadioButton_1, rdbtnNewRadioButton_2,
			rdbtnNewRadioButton_3, rdbtnSekunda, rdbtnPSekundy, rdbtnSekundy;
	private JMenuItem mntmOtworzPlik;
	private ButtonGroup bgElementy, bgCzas;
	private JMenuItem mntmZapiszPlik_1, mntmInstrukcja;
	private JButton btnStart, btnStop, btnNastpnaGeneracja, btnWyczyPlansze;
	private Plansza plansza, plansza2;
	private Wireworld gra;
	private JMenu mnPomoc;
	boolean czyZatrzymac;
	private long czas = 1000;
	private String instrukcja = "Automat kom�rkowy Wireworld\n"
			+ "1. Aby rozpocz�� gre nale�y wybra� jeden z element�w z prawego panelu\n"
			+ "lub wczyta� plik z gotow� generacj� wybieraj�c Opcje-> Otw�rz plik.\n "
			+ "2. Je�eli wczyta�e� plik naci�nij Start aby rozpocz��, aby zatrzyma� naci�nij\n"
			+ "Stop. Je�eli Tw�j wyb�r pad� na elementy, klikaj�c na plansz� mo�esz je\n"
			+ "ustawi� w dowolnym miejsciu.\n"
			+ "3. W polu tekstowym mo�esz wpisa� dowoln� liczb� generacji do uzyskania.\n"
			+ "4. Aby zmieni� interwa� czasowy mi�dzy kolejnymi wy�wietleniami siatki ustaw\n"
			+ "jedn� z trzech opcji widocznych poni�ej planszy\n"
			+ "5. W ka�dej chwili mo�esz zapisa� plik z aktualn� generacj� Opcje -> Zapisz plik.\n"
			+ "6. Aby wyczy�ci� siatk� nale�y przycisn�� przycisk Wyczy�� plansz� ";
	private Obraz panel;

	public Okienko() {

		plansza = new Plansza(60, 60);
		plansza2 = new Plansza(60, 60);
		gra = new Wireworld(plansza);

		bgElementy = new ButtonGroup();
		bgCzas = new ButtonGroup();

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 800, 700);
		setLocation(250, 1);
		setTitle("Automat kom�rkowy Wireworld");

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnOpcje = new JMenu("Opcje");
		menuBar.add(mnOpcje);

		mntmOtworzPlik = new JMenuItem("Otw\u00F3rz plik");
		mnOpcje.add(mntmOtworzPlik);
		mntmOtworzPlik.addActionListener(this);

		mntmZapiszPlik_1 = new JMenuItem("Zapisz plik");
		mnOpcje.add(mntmZapiszPlik_1);
		mntmZapiszPlik_1.addActionListener(this);

		mnPomoc = new JMenu("Pomoc");
		menuBar.add(mnPomoc);

		mntmInstrukcja = new JMenuItem("Instrukcja");
		mnPomoc.add(mntmInstrukcja);
		mntmInstrukcja.addActionListener(this);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		panel = new Obraz(plansza);
		panel.setBackground(Color.BLACK);
		panel.setBounds(0, 0, 600, 600);
		contentPane.add(panel);
		panel.setToolTipText("Plansza");
		panel.addMouseListener(panel);

		btnStart = new JButton("Start");
		btnStart.setBounds(628, 30, 146, 35);
		contentPane.add(btnStart);
		btnStart.addActionListener(this);
		btnStart.setToolTipText("Naci�nij aby rozpocz��");

		btnStop = new JButton("Stop");
		btnStop.setBounds(628, 76, 146, 35);
		contentPane.add(btnStop);
		btnStop.addActionListener(this);
		btnStop.setToolTipText("Naci�nij aby zatrzyma�");

		btnNastpnaGeneracja = new JButton("Nast\u0119pna generacja");
		btnNastpnaGeneracja.setBounds(628, 122, 146, 35);
		contentPane.add(btnNastpnaGeneracja);
		btnNastpnaGeneracja.addActionListener(this);

		JLabel lblLiczbaGeneracji = new JLabel("LIczba generacji:");
		lblLiczbaGeneracji.setBounds(638, 182, 136, 26);
		contentPane.add(lblLiczbaGeneracji);

		textField = new JTextField();
		textField.setBounds(636, 214, 138, 35);
		contentPane.add(textField);
		textField.setColumns(10);
		textField.setToolTipText("Liczba generacji");
		textField.setText("500");

		JLabel lblElementy = new JLabel("Elementy:");
		lblElementy.setBounds(638, 269, 82, 16);
		contentPane.add(lblElementy);

		btnWyczyPlansze = new JButton("Wyczy\u015B\u0107 plansze");
		btnWyczyPlansze.setBounds(628, 549, 146, 35);
		contentPane.add(btnWyczyPlansze);
		btnWyczyPlansze.addActionListener(this);

		JLabel lblPokaZInterwaem = new JLabel(
				"Poka\u017C z interwa\u0142em mi\u0119dzy ruchami:");
		lblPokaZInterwaem.setBounds(10, 611, 206, 16);
		contentPane.add(lblPokaZInterwaem);

		rdbtnNewRadioButton = new JRadioButton("G\u0142owa", false);
		rdbtnNewRadioButton.setBounds(612, 297, 65, 18);
		contentPane.add(rdbtnNewRadioButton);
		rdbtnNewRadioButton.addActionListener(this);
		bgElementy.add(rdbtnNewRadioButton);

		rdbtnNewRadioButton_1 = new JRadioButton("Ogon", false);
		rdbtnNewRadioButton_1.setBounds(612, 327, 65, 18);
		contentPane.add(rdbtnNewRadioButton_1);
		rdbtnNewRadioButton_1.addActionListener(this);
		bgElementy.add(rdbtnNewRadioButton_1);

		rdbtnNewRadioButton_2 = new JRadioButton("Przewodnik", true);
		rdbtnNewRadioButton_2.setBounds(689, 327, 88, 18);
		contentPane.add(rdbtnNewRadioButton_2);
		rdbtnNewRadioButton_2.addActionListener(this);
		bgElementy.add(rdbtnNewRadioButton_2);

		rdbtnNewRadioButton_3 = new JRadioButton("Pusta", false);
		rdbtnNewRadioButton_3.setBounds(689, 297, 88, 18);
		contentPane.add(rdbtnNewRadioButton_3);
		rdbtnNewRadioButton_3.addActionListener(this);
		bgElementy.add(rdbtnNewRadioButton_3);

		JLabel lblBamkiLogiczne = new JLabel("Bamki logiczne:");
		lblBamkiLogiczne.setBounds(640, 373, 121, 16);
		contentPane.add(lblBamkiLogiczne);

		rdbtnNewRadioButton_4 = new JRadioButton("Diode", false);
		rdbtnNewRadioButton_4.setBounds(612, 401, 115, 18);
		contentPane.add(rdbtnNewRadioButton_4);
		rdbtnNewRadioButton_4.addActionListener(this);
		bgElementy.add(rdbtnNewRadioButton_4);

		rdbtnNewRadioButton_5 = new JRadioButton("OR", false);
		rdbtnNewRadioButton_5.setBounds(612, 431, 115, 18);
		contentPane.add(rdbtnNewRadioButton_5);
		rdbtnNewRadioButton_5.addActionListener(this);
		bgElementy.add(rdbtnNewRadioButton_5);

		rdbtnNewRadioButton_6 = new JRadioButton("AND - NOT", false);
		rdbtnNewRadioButton_6.setBounds(612, 461, 115, 18);
		contentPane.add(rdbtnNewRadioButton_6);
		rdbtnNewRadioButton_6.addActionListener(this);
		bgElementy.add(rdbtnNewRadioButton_6);

		rdbtnNewRadioButton_7 = new JRadioButton("FLIP - FLOP", false);
		rdbtnNewRadioButton_7.setBounds(612, 491, 115, 18);
		contentPane.add(rdbtnNewRadioButton_7);
		rdbtnNewRadioButton_7.addActionListener(this);
		bgElementy.add(rdbtnNewRadioButton_7);

		rdbtnSekunda = new JRadioButton("1 sekunda", true);
		rdbtnSekunda.setBounds(227, 612, 88, 18);
		contentPane.add(rdbtnSekunda);
		rdbtnSekunda.addActionListener(this);
		bgCzas.add(rdbtnSekunda);

		rdbtnPSekundy = new JRadioButton("p\u00F3\u0142 sekundy", false);
		rdbtnPSekundy.setBounds(327, 612, 100, 18);
		contentPane.add(rdbtnPSekundy);
		rdbtnPSekundy.addActionListener(this);
		bgCzas.add(rdbtnPSekundy);

		rdbtnSekundy = new JRadioButton("1/8 sekundy", false);
		rdbtnSekundy.setBounds(439, 612, 100, 18);
		contentPane.add(rdbtnSekundy);
		rdbtnSekundy.addActionListener(this);
		bgCzas.add(rdbtnSekundy);

		czyZatrzymac = false;

		try {
			UIManager
					.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (ClassNotFoundException | InstantiationException
				| IllegalAccessException | UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		}
		SwingUtilities.updateComponentTreeUI(this);

	}

	@Override
	public void actionPerformed(ActionEvent e) {
		Object wybrane = e.getSource();

		if (wybrane == mntmOtworzPlik) {
			JFileChooser fc = new JFileChooser();
			if (fc.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				File plik = fc.getSelectedFile();
				OdczytZapis wczytaj = new OdczytZapis();
				try {
					plansza = wczytaj.czytajPlik(plik);
					panel.ustawiajPanel(plansza);
				} catch (IOException ew) {
					Logger.getLogger(Okienko.class.getName()).log(Level.SEVERE,
							null, ew);
				}
				gra = new Wireworld(plansza);
				panel.repaint();
			}
		} else if (wybrane == mntmZapiszPlik_1) {
			JFileChooser fh = new JFileChooser();
			if (fh.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
				File plik = fh.getSelectedFile();
				OdczytZapis zapisz = new OdczytZapis();
				try {
					zapisz.zapisujPlik(plansza, plik);
				} catch (IOException ew) {
					Logger.getLogger(Okienko.class.getName()).log(Level.SEVERE,
							null, ew);
				}
			}
		} else if (wybrane == mntmInstrukcja) {
			JOptionPane.showMessageDialog(mnPomoc, instrukcja);
		} else if (wybrane == btnStart) {

			czyZatrzymac = false;
			new Thread() {
				@Override
				public void run() {
					long liczbaGeneracji = Long.parseLong(textField.getText());
					while (liczbaGeneracji > 0 && czyZatrzymac == false ) {
						plansza2 = gra.nastepnaGeneracja(plansza);
						plansza = plansza2.kopjuj(plansza2);
						panel.ustawiajPanel(plansza);
						panel.repaint();
						liczbaGeneracji--;
						textField.setText(String.valueOf(liczbaGeneracji));
						try {
							Thread.sleep(czas);
						} catch (InterruptedException ew) {
							Logger.getLogger(Okienko.class.getName()).log(
									Level.SEVERE, null, ew);
						}
					}
				}
			}.start();

		} else if (wybrane == btnStop) {
			czyZatrzymac = true;
		} else if (wybrane == btnWyczyPlansze) {
			plansza.wyczyscPlansze();
			panel.ustawiajPanel(plansza);
			panel.repaint();
		} else if (wybrane == btnNastpnaGeneracja) {
			plansza2 = gra.nastepnaGeneracja(plansza);
			plansza = plansza2.kopjuj(plansza2);
			panel.ustawiajPanel(plansza);
			panel.repaint();
			int liczbaGeneracj = Integer.parseInt(textField.getText());
			liczbaGeneracj--;
			textField.setText(String.valueOf(liczbaGeneracj));
		} else if (wybrane == rdbtnSekunda) {
			czas = 1000;
		} else if (wybrane == rdbtnSekundy) {
			czas = 125;
		} else if (wybrane == rdbtnPSekundy) {
			czas = 500;
		} else if (wybrane == rdbtnNewRadioButton_4) {
			panel.ustawiajElement(new Dioda());
			panel.wybierajBramke();
		} else if (wybrane == rdbtnNewRadioButton_5) {
			panel.ustawiajElement(new OR());
			panel.wybierajBramke();
		} else if (wybrane == rdbtnNewRadioButton_6) {
			panel.ustawiajElement(new AndNot());
			panel.wybierajBramke();
		} else if (wybrane == rdbtnNewRadioButton_7) {
			panel.ustawiajElement(new FlipFlop());
			panel.wybierajBramke();
		} else if (wybrane == rdbtnNewRadioButton) {
			panel.ustawiajStan(new Glowa());
			panel.wybierajElement();
		} else if (wybrane == rdbtnNewRadioButton_1) {
			panel.ustawiajStan(new Ogon());
			panel.wybierajElement();
		} else if (wybrane == rdbtnNewRadioButton_2) {
			panel.ustawiajStan(new Przewodnik());
			panel.wybierajElement();
		} else if (wybrane == rdbtnNewRadioButton_3) {
			panel.ustawiajStan(new Pusta());
			panel.wybierajElement();

		}

	}

}
