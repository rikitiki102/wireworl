package gui;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPanel;

import wireworld.*;

public class Obraz extends JPanel implements MouseListener {

	private static final long serialVersionUID = 1L;
	Plansza plansza;
	Stan stan = new Przewodnik();
	boolean czyWybranoBramke = false;

	Elementy bramka;

	public Obraz(Plansza pl) {
		setBackground(Color.WHITE);
		this.plansza = pl;
		this.czyWybranoBramke = false;
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		Stan stan;
		for (int i = 0; i < 60; i++) {
			for (int j = 0; j < 60; j++) {
				stan = plansza.pobierajStan(i, j);
				g.setColor(stan.zwracajKolor());
				g.fillRect(i * 10, j * 10, 10, 10);
			}
		}
		for (int i = 0; i < 60; i++) {
			g.setColor(Color.darkGray);
			g.drawLine(i * 10, 0, i * 10, 600);
		}
		for (int i = 0; i < 60; i++) {
			g.setColor(Color.darkGray);
			g.drawLine(0, i * 10, 600, i * 10);
		}
	}

	public void ustawiajPanel(Plansza pl) {
		this.plansza = pl;
	}

	public void ustawiajStan(Stan st) {
		stan = st;
	}

	public Plansza zwracajPlansze() {
		return this.plansza;
	}

	public void wybierajBramke() {
		this.czyWybranoBramke = true;
	}

	public void wybierajElement() {
		this.czyWybranoBramke = false;
	}

	public void ustawiajElement(Elementy element) {
		bramka = element;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		if (!czyWybranoBramke) {
			plansza.siatka[(int) x / 10][(int) y / 10].ustawiajStan(stan);
			repaint();
			this.ustawiajPanel(plansza);
		} else {
			bramka.narysujElement((int) x / 10, (int) y / 10, plansza);
			repaint();
			this.ustawiajPanel(plansza);
		}
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		int x = e.getX();
		int y = e.getY();
		if (!czyWybranoBramke) {
			plansza.siatka[(int) x / 10][(int) y / 10].ustawiajStan(stan);
			repaint();
			this.ustawiajPanel(plansza);
		} else {
			bramka.narysujElement((int) x / 10, (int) y / 10, plansza);
			repaint();
			this.ustawiajPanel(plansza);
		}
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub

	}

}
